/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dao.daobase;

import java.util.List;
import org.hibernate.Query;

import com.yyl.common.collection.ListData;

/**
 *
 * @author admin
 */
public interface CommonDao<Tp> {
    
    public Tp findObject(long id);// CommonDaoTest:

    public List<Tp> findAll();// CommonDaoTest:79

    public List<Tp> findAll(String condition);// CommonDaoTest:147

    public List<Tp> findAll(int start, int offset);// CommonDaoTest:132

    public List<Tp> findAll(int start, int offset, String condition);

    public List<Tp> findAll(String condition, Object... argv);

    public List<Tp> findAll(int start, int offset, String condition, Object... argv);

    public ListData<Tp> list(int start,int offset);

    public ListData<Tp> list(int start, int offset, String condition, Object... argv);

    public int removeAll();// CommonDaoTest:15

    public int removeAll(String condition, Object... argv);// CommonDaoTest:15

    public int removeObject(List<Tp> target);// CommonDaoTest:58

    public void removeObject(Tp object);// CommonDaoTest:31

    public List<Tp> addObject(List<Tp> list);// CommonDaoTest:66

    public Tp saveOrUpdateObject(Tp object);

    public Tp addObject(Tp object);// CommonDaoTest:79

    public Tp forceAddObject(Object obj);

    public Tp mergeObject(Tp object);// CommonDaoTest:105

    public Tp forceMergeObject(Object obj);

    public int count();// CommonDaoTest:162

    public int count(String condition);// CommonDaoTest:169

    public int count(String condition, Object... argv);

    public Tp getReference(long id);

    public void refreshObject(Tp object);

    public void flushContext();

    public List<Tp> findLast(int n);

    public List<Tp> findLast(String condition, int n);

    public List<Tp> findLast(String condition, int n, Object... argv);

    public Tp findOne();

    public Tp findOne(String condition);

    public Tp findOne(String condition, Object... argv);

    public Query query(String query);

    public Query query(String query, Object... argv);

//    public Query query(CriteriaQuery<Tp> query);
//
//    public CriteriaQuery<Tp> createCriteriaQuery();

    public Query nativeQuery(String query, Object... argv);

    public Class<? extends Tp> getEntityClass();

    public String getEntityName();

    public void beginTransaction();

    public void commitTransaction();

    public void rollbackTransaction();
}
