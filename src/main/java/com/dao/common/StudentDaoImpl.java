package com.dao.common;

import org.springframework.stereotype.Repository;

import com.dao.daobase.DaoBase;
import com.pojo.Student;

@Repository("studentDao")
public class StudentDaoImpl extends DaoBase<Student>{

}
