package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.daobase.CommonDao;
import com.pojo.Student;
import com.yyl.common.collection.ListData;
import com.yyl.common.entity.BaseException;

@Service("studentService")
public class StudentServiceImpl implements StudentService{

	@Autowired
	private CommonDao<Student> studentDao;

	@Override
	public Student getStudentById(Long id) throws BaseException {
		// TODO Auto-generated method stub
		Student tbUser = studentDao.findObject(id);
		if (tbUser == null) {
			throw new BaseException("NOT EXISTS");
		}
		return tbUser;
	}

	@Override
	public ListData<Student> listStudents(int start, int limit) {
		// TODO Auto-generated method stub
		return studentDao.list(start, limit);
	}

	@Override
	public Student deleteStudentById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Student updateStudentById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
