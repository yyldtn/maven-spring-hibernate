package com.service;

import com.pojo.Student;
import com.yyl.common.collection.ListData;
import com.yyl.common.entity.BaseException;

public interface StudentService {

	Student getStudentById(Long id) throws BaseException;
	
	ListData<Student> listStudents(int start, int limit);
	
	Student deleteStudentById(Long id);
	
	Student updateStudentById(Long id);
	
	
}
