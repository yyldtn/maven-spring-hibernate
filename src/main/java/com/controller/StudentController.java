package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pojo.Student;
import com.service.StudentService;
import com.yyl.common.collection.ListData;
import com.yyl.common.entity.BaseException;

@Controller
public class StudentController {

	@Autowired
	private StudentService studentService;
	
	@RequestMapping(value="/student/{studentId}", method = RequestMethod.GET)
	@ResponseBody
	Student getUserById(@PathVariable Long studentId) throws BaseException{
		
		return studentService.getStudentById(studentId);
	}
	
	@RequestMapping(value="/student", method = RequestMethod.GET)
	@ResponseBody
	ListData<Student> listStudents(int start, int limit) {
		
		return studentService.listStudents(start, limit);
	}
}
